<?php

namespace Tests\Unit\Domain\Usecase;

use PHPUnit\Framework\TestCase;
use Zinio\Domain\Model\City;
use Zinio\Domain\Usecase\CitiesOrganizerOutput;

class CitiesOrganizerOutputTest extends TestCase {
    public function testOutput()
    {
        $cities = [
            new City('Test1', 1.1, 1.2),
            new City('Test2', 1.8, 1.8),
            new City('Test3', 1.3, 1.4)
        ];

        $expectedResult = 'Test1' . PHP_EOL . 'Test2' . PHP_EOL . 'Test3' . PHP_EOL;

        $output = new CitiesOrganizerOutput();
        $result = $output->citiesToString($cities);

        $this->assertSame($expectedResult, $result);
    }
}