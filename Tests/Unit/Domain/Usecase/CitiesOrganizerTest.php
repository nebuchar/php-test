<?php

namespace Tests\Unit\Domain\Usecase;

use PHPUnit\Framework\TestCase;
use Zinio\Domain\Model\City;
use Zinio\Domain\Usecase\CitiesOrganizer;

class CitiesOrganizerTest extends TestCase {
    public function testOrganize()
    {
        $cities = [
            new City('Test1', 1.1, 1.2),
            new City('Test2', 1.8, 1.8),
            new City('Test3', 1.3, 1.4)
        ];
        $expectedCities = [
            new City('Test1', 1.1, 1.2),
            new City('Test3', 1.3, 1.4),
            new City('Test2', 1.8, 1.8)
        ];

        $usecase = new CitiesOrganizer();
        $result = $usecase->organize($cities);

        $this->assertEquals($expectedCities, $result);
    }
}