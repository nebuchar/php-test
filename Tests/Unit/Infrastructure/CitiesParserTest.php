<?php

namespace Tests\Unit\Infrastructure;

use PHPUnit\Framework\TestCase;
use Zinio\Domain\Model\City;
use Zinio\Infrastructure\citiesParser;

class CitiesParserTest extends TestCase {
    public function testParseContents()
    {
        $contents = 'Test1 1.1 1.2
        Test2 1.3 1.4';
        $expectedResult = [
            new City('Test1', 1.1, 1.2),
            new City('Test2', 1.3, 1.4)
        ];

        $parser = new citiesParser();
        $result = $parser->parseContents($contents);

        $this->assertEquals($expectedResult, $result);
    }
}