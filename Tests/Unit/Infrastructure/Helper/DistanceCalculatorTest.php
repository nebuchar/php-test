<?php

namespace Tests\Unit\Infrastructure\Helper;

use PHPUnit\Framework\TestCase;
use Zinio\Domain\Model\City;
use Zinio\Infrastructure\Helper\DistanceCalculator;

class DistanceCalculatorTest extends TestCase{
    public function testCalculate()
    {

        $city1 = new City('Test1', 1.1, 1.2);
        $city2 = new City('Test2', 1.3, 1.4);

        $result = DistanceCalculator::calculateDistance($city1->getLat(), $city1->getLong(), $city2->getLat(), $city2->getLong());

        $this->assertTrue(is_float($result));
    }
}