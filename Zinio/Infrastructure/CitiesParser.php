<?php

namespace Zinio\Infrastructure;

use Zinio\Domain\Model\City;

class citiesParser {

    /**
     * @param string $fileContent
     * @return array
     */
    public function parseContents($fileContent)
    {
        $rows = explode(PHP_EOL, $fileContent);
        $cities = [];

        foreach ($rows as $row) {

            $rowData = explode(' ', trim($row));
            $cities[] = new City($rowData[0], (float)$rowData[1], (float)$rowData[2]);
        }

        return $cities;
    }

    /**
     * @param string $path
     * @return bool|string
     */
    public function getFileContents($path)
    {
        return file_get_contents($path);
    }


}