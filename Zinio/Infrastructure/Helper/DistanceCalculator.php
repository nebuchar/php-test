<?php

namespace Zinio\Infrastructure\Helper;

class DistanceCalculator {
    /**
     * @param float $lat1
     * @param float $long1
     * @param float $lat2
     * @param float $long2
     * @return float
     */
    static public function calculateDistance($lat1, $long1, $lat2, $long2){
         return rad2deg(acos((sin(deg2rad($lat1))*sin(deg2rad($lat2))) + (cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($long1-$long2)))));
    }
}