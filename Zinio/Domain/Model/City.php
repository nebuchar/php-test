<?php

namespace Zinio\Domain\Model;

class City {
    /** @var string */
    private $name;
    /** @var float */
    private $lat;
    /** @var float */
    private $long;

    /**
     * City constructor.
     * @param string $name
     * @param float $lat
     * @param float $long
     */
    public function __construct($name, $lat, $long)
    {
        $this->name = $name;
        $this->lat = $lat;
        $this->long = $long;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLong()
    {
        return $this->long;
    }
}