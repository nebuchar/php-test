<?php

namespace Zinio\Domain\Usecase;

use Zinio\Domain\Model\City;

class CitiesOrganizerOutput {
    /**
     * @param City[] $cities
     * @return string
     */
    public function citiesToString($cities)
    {
        $output = '';
        if(!empty($cities)) {
            foreach ($cities as $city) {
                $output .= $city->getName() . PHP_EOL;
            }
        }
        return $output;
    }
}