<?php

namespace Zinio\Domain\Usecase;

use Zinio\Domain\Model\City;
use Zinio\Infrastructure\Helper\DistanceCalculator;

class CitiesOrganizer {

    /** @var City */
    private $currentCity;

    public function execute()
    {

    }

    /**
     * @param City[] $cities
     * @return City[]
     */
    public function organize($cities)
    {
        $orderecCities = [];

        if (!empty($cities)) {
            $this->currentCity = $cities[0];
            //$orderecCities[] = $cities[0];
            //unset($cities[0]);
        }

        usort($cities, [$this, 'compareDistance']);

        return $cities;
    }

    /**
     * @param City $city1
     * @param City $city2
     * @return int
     */
    private function compareDistance($city1, $city2) {
        $dist1 = DistanceCalculator::calculateDistance($this->currentCity->getLat(), $this->currentCity->getLong(), $city1->getLat(), $city1->getLong());
        $dist2 = DistanceCalculator::calculateDistance($this->currentCity->getLat(), $this->currentCity->getLong(), $city2->getLat(), $city2->getLong());

        if($dist1 == $dist2) {
            return 0;
        }
        return ($dist1 < $dist2) ? -1 : 1;
    }
}