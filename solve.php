<?php

require 'vendor/autoload.php';

use \Zinio\Infrastructure\citiesParser;
use \Zinio\Domain\Usecase\CitiesOrganizer;
use \Zinio\Domain\Usecase\CitiesOrganizerOutput;

$citiesParser = new citiesParser();
$cities = $citiesParser->parseContents($citiesParser->getFileContents('./cities.txt'));

$usecase = new CitiesOrganizer();
$cities = $usecase->organize($cities);

$output = new CitiesOrganizerOutput();

echo $output->citiesToString($cities);